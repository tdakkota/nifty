#include "tokenize.h"

#include <stdbool.h>

#include "friendly_errors.h"
#include "string.h"

static bool is_digit(char c) {
    return (c >= '0' && c <= '9');
}

static bool isalphanum(char c) {
    return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') ||
           (c >= 'a' && c <= 'z');
}

static bool is_allowed_in_symbol(char c) {
    // Include anyting but parentheses and strlit start
    return c >= '!' && c <= '~' && c != '\'' && c != '(' && c != ')';
}

// vec of Token
vec tokenize(const char *s) {
    vec toks = vecNew();

    for (size_t i = 0; s[i] != '\0';) {
        if (s[i] == ' ') {
            i += 1;
        } else if (s[i] == '\n') {
            i += 1;
        } else if (s[i] == '(') {
            vecPush(toks,
                    ((Token) {.tag=TOKEN_TAG_PAREN_L, .s_start=i, .s_end=(
                        i + 1)}), Token);
            i += 1;
        } else if (s[i] == ')') {
            vecPush(toks,
                    ((Token) {.tag = TOKEN_TAG_PAREN_R, .s_start=i, .s_end=(
                        i + 1)}), Token);
            i += 1;
        } else if (is_digit(s[i])) {
            u64 nat = 0;
            size_t number_length = 0;
            do {
                nat = nat * 10 + (u64) (s[i + number_length] - '0');
                number_length += 1;
            } while (is_digit(s[i + number_length]));
            vecPush(toks,
                    ((Token) {.tag = TOKEN_TAG_NAT, .v.nat = nat, .s_start=i, .s_end=(
                        i + number_length)}),
                    Token);
            i += number_length;
        } else if (is_allowed_in_symbol(s[i])) {
            vec symbol = vecNew(); // vec of char
            size_t symbol_length = 0;
            do {
                vecPush(symbol, s[i + symbol_length], char);
                symbol_length += 1;
            } while (is_allowed_in_symbol(s[i + symbol_length]));
            vecPush(symbol, '\0', char);
            vecPush(toks,
                    ((Token) {.tag = TOKEN_TAG_SYMBOL, .v.symbol = symbol, .s_start=i, .s_end=(
                        i + symbol_length)}),
                    Token);
            i += symbol_length;
        } else if (s[i] == '\'') {
            vec strlit = vecNew(); // vec of char
            size_t strlit_length = 0;

            i += 1;
            while (s[i + strlit_length] != '\'') {
                if (s[i + strlit_length] == '\0') {
                    panic("EOF reached when scanning a string literal\n");
                }

                if (s[i + strlit_length] == '\\') {
                    // Escape sequence
                    if (s[i + strlit_length + 1] == 'n') {
                        vecPush(strlit, '\n', char);
                    } else {
                        panic("Unknown escape sequence!\n");
                    }
                    strlit_length += 2;
                } else {
                    vecPush(strlit, s[i + strlit_length], char);
                    strlit_length += 1;
                }
            }
            strlit_length += 1;
            vecPush(strlit, '\0', char);
            vecPush(toks,
                    ((Token) {.tag = TOKEN_TAG_STRLIT, .v.strlit = strlit, .s_start=i, .s_end=(
                        i + strlit_length)}),
                    Token);
            i += strlit_length;
        } else {
            print_error_in_text_at_char(s, i, i, "Unknown character.\n");
        }
    }

    return toks;
}

void free_toks(vec toks) {
    for (size_t i = 0; i < toks.size; i++) {
        if (vecGet(toks, i, Token).tag == TOKEN_TAG_SYMBOL) {
            vecDestroy(vecGet(toks, i, Token).v.symbol);
        } else if (vecGet(toks, i, Token).tag == TOKEN_TAG_STRLIT) {
            vecDestroy(vecGet(toks, i, Token).v.strlit);
        }
    }
    vecDestroy(toks);
}
