#include "help2.h"

char *vec_char_to_string(vec st) {
    return strcpy(malloc(st.size), st.data);
}

vec string_to_vec_char(const char *str) {
    size_t len = strlen(str) + 1;
    return (vec) {.size=len, .data=strcpy(malloc(len), str)};
}
