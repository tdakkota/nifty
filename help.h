#pragma once

#include <stdlib.h>
#include <memory.h>

void *malloc_memcpy(void *p, size_t size);

char *clone_str(const char *str);
