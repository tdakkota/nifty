#include "friendly_errors.h"

#include <stdio.h>
#include <stdnoreturn.h>

#include "vec.h"

noreturn void
print_error_in_text_at_char(const char *string, size_t target_start,
                            size_t target_end,
                            const char *error) {
    size_t line_number = 0;
    size_t line_start_index = 0;
    size_t line_end_index = 0;

    // Count \n-s to ther left
    for (size_t i = target_start;; --i) {
        if (string[i] == '\n') {
            line_number += 1;
            break;
        }
        if (i == 0) {
            break;
        };
    }

    // Find an \n to ther left
    for (size_t i = target_start;; --i) {
        if (string[i] == '\n') {
            line_start_index = i + 1;
            break;
        }
        if (i == 0) {
            line_start_index = i;
            break;
        };
    }

    // Find an \n to ther right
    for (size_t i = target_end;; ++i) {
        if (string[i] == '\n') {
            line_end_index = i;
            break;
        }
        if (string[i] == '\0') {
            line_end_index = i;
            break;
        };
    }

    size_t position_of_target_start_in_line = target_start - line_start_index;
    size_t position_of_target_end_in_line = target_end - line_start_index;

    printf("%s", error);

    // Output linenumber_column
    int linenumber_column_width;
    printf("  %lu | %n", line_number + 1, &linenumber_column_width);
    // Output line with target
    for (size_t i = line_start_index; i < line_end_index; ++i) {
        if (string[i] != '\n') {
            putc(string[i], stdout);
        } else {
            putc('\\', stdout);
        }
    }
    putc('\n', stdout);

    for (size_t i = 0; i < (size_t) linenumber_column_width +
                           position_of_target_start_in_line; ++i) {
        putc(' ', stdout);
    }
    putc('^', stdout);
    for (size_t i = 0; i < position_of_target_end_in_line -
                           position_of_target_start_in_line - 1; ++i) {
        putc('~', stdout);
    }
    putc('\n', stdout);

    exit(1);
}
