#include "interpret.h"
#include "help2.h"
#include "NValue.h"
#include "compile.h"

static IRExpr beta_reduce(IRExpr expr, vec arg_name, NValue nv) {
    if (expr.tag == EXPR_TAG_VALUE) {
        // Check if it should be substituted
        if (strcmp(expr.v.value.type.data, "symbol") == 0 &&
            strcmp(expr.v.value.value.symbol.data, arg_name.data) == 0) {
            return (IRExpr) {.tag=EXPR_TAG_VALUE, .v.value=clone_NValue(nv)};
        } else {
            return clone_ir_expr(expr);
        }
    } else if (expr.tag == EXPR_TAG_FUNC_CALL) {
        IRFuncCall *fc = malloc(sizeof(IRFuncCall));
        fc->func_expr = beta_reduce(expr.v.func_call->func_expr, arg_name, nv);
        fc->arg = beta_reduce(expr.v.func_call->arg, arg_name, nv);

        return (IRExpr) {.tag=EXPR_TAG_FUNC_CALL, .v.func_call=fc};
    } else if (expr.tag == EXPR_TAG_LAMBDA_DEF) {
        IRLambdaDef *ld = malloc(sizeof(IRLambdaDef));
        ld->arg_name = vecClone(expr.v.lambda_def->arg_name);
        ld->body = beta_reduce(expr.v.lambda_def->body, arg_name, nv);

        return (IRExpr) {.tag=EXPR_TAG_LAMBDA_DEF, .v.lambda_def=ld};
    } else if (expr.tag == EXPR_TAG_GET_GLOBAL) {
        return (IRExpr) {.tag=EXPR_TAG_GET_GLOBAL, .v.get_global=vecClone(
            expr.v.get_global)};
    } else {
        panic("Beta reduction: unknown expr.tag error.\n");
    }
}

static NValue interpret_expr(IRExpr expr) {
    if (expr.tag == EXPR_TAG_VALUE) {
        return clone_NValue(expr.v.value);
    } else if (expr.tag == EXPR_TAG_FUNC_CALL) {
        IRFuncCall *fc = expr.v.func_call;

        NValue nv1 = interpret_expr(fc->func_expr);
        NValue nv2 = interpret_expr(fc->arg);

        if (strcmp(nv1.type.data, "func") == 0) {
            IRExpr beta_reduction_result = beta_reduce(
                *(IRExpr *) nv1.value.function.expr,
                nv1.value.function.arg_name, nv2);
            NValue interpretation_result = interpret_expr(
                beta_reduction_result);
            free_ir_expr(beta_reduction_result);

            free_NValue(nv2); // TODO: remove this when GC is added
            free_NValue(nv1); // TODO: remove this when GC is added
            return interpretation_result;
        } else if (strcmp(nv1.type.data, "nativefunc") == 0) {
            if (strcmp(nv1.value.native_func.name.data, "print") == 0) {
                print_NValue(nv2);

                free_NValue(nv2); // TODO: remove this when GC is added
                free_NValue(nv1); // TODO: remove this when GC is added
                return (NValue) {.type=string_to_vec_char("unit")};
            } else if (strcmp(nv1.value.native_func.name.data, "add") == 0) {
                NValue result;
                if (!nv1.value.native_func.data.add.is_left_filled) {
                    NVNativeFunction nvnf = {.name=string_to_vec_char(
                        "add"), .data.add.is_left_filled=true, .data.add.left=nv2.value.nat};
                    result = (NValue) {.type=string_to_vec_char(
                        "nativefunc"), .value.native_func=nvnf};
                } else {
                    u64 summ =
                        nv1.value.native_func.data.add.left + nv2.value.nat;
                    result = (NValue) {.type=string_to_vec_char(
                        "nat"), .value.nat=summ};
                }
                free_NValue(nv2); // TODO: remove this when GC is added
                free_NValue(nv1); // TODO: remove this when GC is added
                return result;
            } else {
                panic("Unknown native function.\n");
            }
        } else {
            fprintf(stderr, "Interpreter: %s object is not a function\n",
                    (char *) nv1.type.data);
            panic("Interpreter:type error!\n");
        }
    } else if (expr.tag == EXPR_TAG_LAMBDA_DEF) {
        IRExpr *expr2 = malloc(sizeof(IRExpr));
        *expr2 = clone_ir_expr(expr.v.lambda_def->body);
        NVFunction nvf = {.arg_name=vecClone(
            expr.v.lambda_def->arg_name), .expr=(struct IRExpr_t *) expr2};
        return (NValue) {.type=string_to_vec_char("func"), .value.function=nvf};
    } else if (expr.tag == EXPR_TAG_GET_GLOBAL) {
        if (strcmp(expr.v.get_global.data, "print") == 0) {
            NVNativeFunction nvnf = {.name=string_to_vec_char(
                "print")}; // No NVNativeFunction.data
            return (NValue) {.type=string_to_vec_char(
                "nativefunc"), .value.native_func=nvnf};
        } else if (strcmp(expr.v.get_global.data, "add") == 0) {
            NVNativeFunction nvnf = {.name=string_to_vec_char(
                "add"), .data.add.left=0, .data.add.is_left_filled=false};
            return (NValue) {.type=string_to_vec_char(
                "nativefunc"), .value.native_func=nvnf};
        } else {

            // TODO: global variables

            panic("A)\n");
        }
    } else {
        panic("Interpreter error!\n");
    }
}

NValue interpret(IRProgram program) {
    return interpret_expr(program.expr);
}
