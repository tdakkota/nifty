#pragma once

#include "ints.h"
#include "vec.h"

typedef struct {
    char *key;
    u64 value;
} Pair;

typedef struct {
    vec pairs[256]; // array of vecs of Pair-s
} Hashmap;

Hashmap hashmapNew(void);

void hashmapAdd(Hashmap *self, const char *key, u64 v);

u64 hashmapGet(Hashmap *self, const char *key);

void hashmapDestroy(Hashmap *self);
