#pragma once

#include <stdbool.h>

#include "vec.h"

struct IRExpr_t;

typedef struct {
    vec arg_name; // vec of char
    struct IRExpr_t *expr;
} NVFunction;

typedef struct {
    vec name; // vec of char
    union {
        struct {
            u64 left;
            bool is_left_filled;
        } add; // name="add"
    } data;
} NVNativeFunction;

typedef struct {
    vec type; // vec of char
    union {
        u64 nat;
        vec symbol;
        vec strlit;
        NVFunction function; // type="func"
        NVNativeFunction native_func; // type="nativefunc"
        void *data;
    } value;
} NValue;

typedef struct {
    vec name; // vec of char
    NValue nv;
} BindPair;

NValue clone_NValue(NValue nv);

void print_NValue(NValue n_value);

void free_NValue(NValue nv);
