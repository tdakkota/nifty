//
// Created by serid on 9/8/20.
//

#include <stdio.h>

#include "vec.h"
#include "parse.h"
#include "tokenize.h"
#include "hashmap.h"
#include "compile.h"
#include "interpret.h"
#include "nifty.h"

int main(void) {
    Nifty nifty = newNifty();
    // Test
    const char *test_string = "(let a = 10 in\n(let b = 20 in\n(let summ = (add a b) in\n(print summ))))\n";
    printf("Program:\n%s\n", test_string);
    run_str(&nifty, test_string);

    // CLI
    /*{
        printf("Input: ");

        size_t size;
        char *buf = malloc(59);
        getline(&buf, &size, stdin);

        run_str(&nifty, buf);

        free(buf);
    }*/

    return 0;
}
