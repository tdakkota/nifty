#pragma once

#include <stdio.h>

#include "ints.h"
#include "vec.h"

#define ASTNODE_TAG_NAT 1
#define ASTNODE_TAG_SYMBOL 2
#define ASTNODE_TAG_STRLIT 3
#define ASTNODE_TAG_LIST 4

typedef struct {
    vec v; // vec of ASTNode-s
} ASTList;

typedef struct {
    size_t s_start; // Where node starts in input
    size_t s_end; // Where node ends in input
    u8 tag;
    union {
        u64 nat; // ASTNODE_TAG_NAT
        vec symbol; // ASTNODE_TAG_SYMBOL // vec of chars
        vec strlit; // ASTNODE_TAG_STRLIT // vec of chars
        ASTList list; // ASTNODE_TAG_LIST
    } v;
} ASTNode;

ASTNode parse(vec toks);

void print_ast(ASTNode node);

void free_node(ASTNode node);
