#pragma once

#include <stdlib.h>

#include "ints.h"
#include "panic.h"
#include "help.h"

typedef struct {
    size_t size;
    void *data;
} vec;

#define vecNew() \
    (vec) { .size = 0, .data = NULL }

// Returns vec
#define vecNewWithSize(VP__size, VP__T) \
    ({ \
        size_t size = VP__size; \
        (vec) { .size = size, .data = assert_pnnull(malloc(size * sizeof(VP__T)), "Vec creation failed") }; \
    })

#define vecPush(VP__obj, VPush__e, VP__T) \
    vecResize(VP__obj, VP__obj.size + 1, VP__T); \
    ((VP__T*)(VP__obj.data))[VP__obj.size - 1] = VPush__e;

// Returns VP__T
#define vecPop(VP__obj, VP__T) \
    ({ \
        VP__T VP__result = ((VP__T*)(VP__obj.data))[VP__obj.size - 1]; \
        vecResize(VP__obj, VP__obj.size - 1, VP__T); \
        VP__result; \
    })

#define vecResize(VP__obj, VP__size, VP__T) \
    do { \
        size_t size = VP__size; \
        if (size == 0) { \
            free(VP__obj.data); \
            VP__obj.data = NULL; \
        } else { \
            VP__obj.data = assert_pnnull(realloc(VP__obj.data, size * sizeof(VP__T)), "Resize failed"); \
        } \
        VP__obj.size = size; \
    } while(0)

// Can be used for both getting and setting values
#define vecGet(VP__obj, VP__index, VP__T) \
    ((VP__T*)VP__obj.data)[VP__index]

#define vecClone(VP__obj) \
    (vec) {.size = VP__obj.size, .data = malloc_memcpy(VP__obj.data, VP__obj.size)}

#define vecDestroy(VP__obj) \
    free(VP__obj.data); \
    VP__obj.data = NULL;
