#pragma once

#include "vec.h"

char *vec_char_to_string(vec st);

vec string_to_vec_char(const char *str);
