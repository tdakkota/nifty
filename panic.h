#include <stdnoreturn.h>

noreturn void panic(const char *msg);

noreturn void panic_errno(const char *msg);

int assert_ne(int v, int antiexpected, const char* msg);

int assert_ne_errno(int v, int antiexpected, const char* msg);

int assert_nm1(int v, const char* msg);

int assert_nm1_errno(int v, const char* msg);

void* assert_pnnull(void* v, const char* msg);

void* assert_pnm1(void* v, const char* msg);

void* assert_pnm1_errno(void* v, const char* msg);
