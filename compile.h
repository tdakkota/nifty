#pragma once

#include "parse.h"
#include "NValue.h"

#define EXPR_TAG_VALUE 1
#define EXPR_TAG_FUNC_CALL 2
#define EXPR_TAG_LAMBDA_DEF 3
#define EXPR_TAG_GET_GLOBAL 4

struct IRFuncCall_t;
struct IRLambdaDef_t;

typedef struct {
    u8 tag;
    union {
        NValue value; // tag EXPR_TAG_VALUE
        struct IRFuncCall_t *func_call; // tag EXPR_TAG_FUNC_CALL
        struct IRLambdaDef_t *lambda_def; // tag EXPR_TAG_LAMBDA_DEF
        vec get_global; // tag EXPR_TAG_GET_GLOBAL // name of global
    } v;
} IRExpr;

typedef struct IRFuncCall_t {
    IRExpr func_expr;
    IRExpr arg;
} IRFuncCall;

typedef struct IRLambdaDef_t {
    vec arg_name; // vec of char
    IRExpr body;
} IRLambdaDef;

typedef struct {
    IRExpr expr;
} IRProgram;

IRProgram compile(const char *input, ASTNode node);

IRExpr clone_ir_expr(IRExpr expr);

void free_ir_expr(IRExpr expr);

void free_ir_program(IRProgram program);

//
//typedef struct {
//    vec cp; // Constant pool // vec of NValue-s
//    vec funcs; // vec of Func-s
//} IRProgram;
//
//typedef struct {
//    vec varname; // vec of char
//    u64 cp_ix;
//} Command_Set;
