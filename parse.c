#include "parse.h"

#include <stdbool.h>

#include "tokenize.h"
#include "help2.h"

static vec i_toks = {};
static bool is_ok = false;
static size_t next_index = 0xffffffffffffffff;

static ASTNode parse_node(size_t index) {
    if (index >= i_toks.size)
        panic("Expected value, found EOF.\n");

    Token i_tok = vecGet(i_toks, index, Token);

    if (i_tok.tag == TOKEN_TAG_PAREN_L) {
        // We are parsing a list now
        index += 1;

        ASTList list = (ASTList) {.v=vecNew()};

        while (vecGet(i_toks, index, Token).tag != TOKEN_TAG_PAREN_R) {
            ASTNode node1 = parse_node(index);
            if (!is_ok)
                panic("Parsing error!\n");

            // Skip parsed tokens
            index = next_index;
            // Add node to current list
            vecPush(list.v, node1, ASTNode);
        }

        // Assert list is closed
        Token tok_paren_r = vecGet(i_toks, index, Token);
        if (tok_paren_r.tag == TOKEN_TAG_PAREN_R) {
            index += 1;
            next_index = index;
            is_ok = true;
            return (ASTNode) {.tag=ASTNODE_TAG_LIST, .v.list=list, .s_start=i_tok.s_start, .s_end=tok_paren_r.s_end};
        } else {
            is_ok = false;
            return (ASTNode) {};
        }
    } else if (i_tok.tag == TOKEN_TAG_NAT) {
        is_ok = true;
        next_index = index + 1;
        return (ASTNode) {.tag=ASTNODE_TAG_NAT, .v.nat=i_tok.v.nat, .s_start=i_tok.s_start, .s_end=i_tok.s_end};
    } else if (i_tok.tag == TOKEN_TAG_SYMBOL) {
        is_ok = true;
        next_index = index + 1;
        return (ASTNode) {.tag=ASTNODE_TAG_SYMBOL, .v.symbol=vecClone(
            i_tok.v.symbol), .s_start=i_tok.s_start, .s_end=i_tok.s_end};
    } else if (i_tok.tag == TOKEN_TAG_STRLIT) {
        is_ok = true;
        next_index = index + 1;
        return (ASTNode) {.tag=ASTNODE_TAG_STRLIT, .v.strlit=vecClone(
            i_tok.v.strlit), .s_start=i_tok.s_start, .s_end=i_tok.s_end};
    } else {
        is_ok = false;
        return (ASTNode) {};
    }
}

ASTNode parse(vec toks) {
    i_toks = toks;
    ASTNode node = parse_node(0);
    if (!is_ok)
        panic("Parsing failed!");
    return node;
}

void print_ast(ASTNode node) {
    if (node.tag == ASTNODE_TAG_NAT) {
        printf("%zu", node.v.nat);
    } else if (node.tag == ASTNODE_TAG_SYMBOL) {
        printf("%s", (char *) node.v.symbol.data);
    } else if (node.tag == ASTNODE_TAG_STRLIT) {
        printf("'%s'", (char *) node.v.strlit.data);
    } else if (node.tag == ASTNODE_TAG_LIST) {
        ASTList list = node.v.list;
        printf("(");
        for (size_t i = 0; i < list.v.size; i++) {
            print_ast(vecGet(list.v, i, ASTNode));
            if ((i + 1) < list.v.size)
                printf(" ");
        }
        printf(")");
    } else {
        panic("Unknown ASTNode\n");
    }
}

void free_node(ASTNode node) {
    if (node.tag == ASTNODE_TAG_LIST) {
        ASTList list = node.v.list;
        for (size_t i = 0; i < list.v.size; i++) {
            free_node(vecGet(list.v, i, ASTNode));
        }
        vecDestroy(list.v);
    } else if (node.tag == ASTNODE_TAG_SYMBOL) {
        vecDestroy(node.v.symbol);
    } else if (node.tag == ASTNODE_TAG_STRLIT) {
        vecDestroy(node.v.strlit);
    }
}
