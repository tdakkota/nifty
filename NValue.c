#include "NValue.h"

#include <stdio.h>

#include "help2.h"
#include "compile.h"

NValue clone_NValue(NValue nv) {
    if (strcmp(nv.type.data, "nat") == 0) {
        return (NValue) {.type=vecClone(nv.type), .value.nat=nv.value.nat};
    } else if (strcmp(nv.type.data, "symbol") == 0) {
        return (NValue) {.type=string_to_vec_char(
            "symbol"), .value.symbol=vecClone(
            nv.value.symbol)};
    } else if (strcmp(nv.type.data, "strlit") == 0) {
        return (NValue) {.type=string_to_vec_char(
            "strlit"), .value.strlit=vecClone(
            nv.value.strlit)};
    } else if (strcmp(nv.type.data, "func") == 0) {
        IRExpr *expr2 = malloc(sizeof(IRExpr));
        *expr2 = clone_ir_expr(*(IRExpr *) nv.value.function.expr);
        NVFunction nvf = {.arg_name=vecClone(
            nv.value.function.arg_name), .expr=(struct IRExpr_t *) expr2};

        return (NValue) {.type=string_to_vec_char("func"), .value.function=nvf};
    } else if (strcmp(nv.type.data, "nativefunc") == 0) {
        NVNativeFunction nvnf;
        if (strcmp(nv.value.native_func.name.data, "add") == 0) {
            nvnf.name = vecClone(
                nv.value.native_func.name);
            nvnf.data.add = nv.value.native_func.data.add;
        } else if (strcmp(nv.value.native_func.name.data, "print") == 0) {
            // No NVNativeFunction.data to copy
            nvnf.name = vecClone(
                nv.value.native_func.name);
        } else {
            panic("A)\n");
        }
        return (NValue) {.type=string_to_vec_char(
            "nativefunc"), .value.native_func=nvnf};
    } else {
        panic("Clone error: unknown value type.\n");
    }
}

void print_NValue(NValue n_value) {
    if (strcmp(n_value.type.data, "nat") == 0) {
        printf("%lu", n_value.value.nat);
        puts("");
    } else if (strcmp(n_value.type.data, "strlit") == 0) {
        printf("%s", (char *) n_value.value.strlit.data);
    } else if (strcmp(n_value.type.data, "unit") == 0) {
        printf("unit");
    } else if (strcmp(n_value.type.data, "func") == 0) {
        printf("func");
    } else if (strcmp(n_value.type.data, "nativefunc") == 0) {
        printf("nativefunc");
    } else {
        panic("Print error!\n");
    }
}

void free_NValue(NValue nv) {
    if (strcmp(nv.type.data, "nat") == 0) {
        vecDestroy(nv.type);
    } else if (strcmp(nv.type.data, "symbol") == 0) {
        vecDestroy(nv.type);
        vecDestroy(nv.value.symbol);
    } else if (strcmp(nv.type.data, "strlit") == 0) {
        vecDestroy(nv.type);
        vecDestroy(nv.value.strlit);
    } else if (strcmp(nv.type.data, "unit") == 0) {
        vecDestroy(nv.type);
    } else if (strcmp(nv.type.data, "func") == 0) {
        vecDestroy(nv.type);
        vecDestroy(nv.value.function.arg_name);
        free_ir_expr(*(IRExpr *) nv.value.function.expr);
        free(nv.value.function.expr);
    } else if (strcmp(nv.type.data, "nativefunc") == 0) {
        vecDestroy(nv.type);
        vecDestroy(nv.value.native_func.name);
    } else {
        panic("Free error.\n");
    }
}
