#include "nifty.h"

#include "tokenize.h"
#include "interpret.h"

Nifty newNifty(void) {
    return (Nifty) {};
}

void run_str(const Nifty *nifty, const char *code_string) {
    vec toks = tokenize(code_string);

    /*
    // Log token starts and ends
    for (size_t i = 0; i < toks.size; ++i) {
        printf("start: %lu, end: %lu\n", vecGet(toks, i, Token).s_start,
               vecGet(toks, i, Token).s_end);
    }
    */

    ASTNode ast = parse(toks);
    free_toks(toks);

    //print_ast(ast);
    //puts("");
    IRProgram irProgram = compile(code_string, ast);
    free_node(ast);

    printf("Program output:\n");
    NValue program_result = interpret(irProgram);
    free_ir_program(irProgram);

    printf("Program result:\n");
    print_NValue(program_result);
    puts("");
    free_NValue(program_result);
}
