#pragma once

#include "vec.h"

#define TOKEN_TAG_PAREN_L 1
#define TOKEN_TAG_PAREN_R 2
#define TOKEN_TAG_NAT 3
#define TOKEN_TAG_SYMBOL 4
#define TOKEN_TAG_STRLIT 5

typedef struct {
    size_t s_start; // Where token starts in input
    size_t s_end; // Where token ends in input
    u8 tag;
    union {
        u64 nat; // TOKEN_TAG_NAT
        vec symbol; // TOKEN_TAG_SYMBOL // vec of char
        vec strlit; // TOKEN_TAG_STRLIT // vec of char
    } v;
} Token;

vec tokenize(const char *s);

void free_toks(vec toks);
