#include "compile.h"

#include "NValue.h"
#include "help2.h"
#include "friendly_errors.h"

static IRExpr compile_expr(const char *input, ASTNode node) {
    if (node.tag == ASTNODE_TAG_NAT) {
        NValue n_value;
        n_value.type = string_to_vec_char("nat");
        n_value.value.nat = node.v.nat;
        return (IRExpr) {.tag=EXPR_TAG_VALUE, .v.value=n_value};
    } else if (node.tag == ASTNODE_TAG_SYMBOL) {
        NValue n_value;
        n_value.type = string_to_vec_char("symbol");
        n_value.value.symbol = vecClone(node.v.symbol);
        return (IRExpr) {.tag=EXPR_TAG_VALUE, .v.value=n_value};
    } else if (node.tag == ASTNODE_TAG_STRLIT) {
        NValue n_value;
        n_value.type = string_to_vec_char("strlit");
        n_value.value.strlit = vecClone(node.v.strlit);
        return (IRExpr) {.tag=EXPR_TAG_VALUE, .v.value=n_value};
    } else if (node.tag == ASTNODE_TAG_LIST) {
        if (node.v.list.v.size < 1)
            print_error_in_text_at_char(input, node.s_start, node.s_end,
                                        "Syntax error: what is this empty list?\n");

        ASTNode item0 = vecGet(node.v.list.v, 0, ASTNode);
        if (item0.tag == ASTNODE_TAG_SYMBOL) {
            if (strcmp(item0.v.symbol.data, "lambda") == 0) {
                // Lambda definition
                if (node.v.list.v.size != 3)
                    print_error_in_text_at_char(input, node.s_start, node.s_end,
                                                "Lambda definition list should have at least 3 items: `lambda`, <argument_name> and <body>\n");
                ASTNode item1 = vecGet(node.v.list.v, 1, ASTNode);
                ASTNode item2 = vecGet(node.v.list.v, 2, ASTNode);

                if (item1.tag != ASTNODE_TAG_SYMBOL)
                    print_error_in_text_at_char(input, item1.s_start,
                                                item1.s_end,
                                                "Argument name should be a symbol!\n");

                IRLambdaDef *ld = malloc(sizeof(IRLambdaDef));
                ld->arg_name = vecClone(item1.v.symbol);
                ld->body = compile_expr(input, item2);

                return (IRExpr) {.tag=EXPR_TAG_LAMBDA_DEF, .v.lambda_def=ld};
            } else if (strcmp(item0.v.symbol.data, "getg") == 0) {
                // Get global
                if (node.v.list.v.size != 2)
                    print_error_in_text_at_char(input, node.s_start, node.s_end,
                                                "Get global list should have exactly 2 items: `get_global` and <global_name>.\n");
                ASTNode item1 = vecGet(node.v.list.v, 1, ASTNode);
                return (IRExpr) {.tag=EXPR_TAG_GET_GLOBAL, .v.get_global=vecClone(
                    item1.v.symbol)};
            } else if (strcmp(item0.v.symbol.data, "let") == 0) {
                // Let binding
                //
                // let v = e1 in e2
                //
                // is syntactic sugar for
                //
                // (lambda v e2) e1

                if (node.v.list.v.size != 6)
                    print_error_in_text_at_char(input, node.s_start, node.s_end,
                                                "Let binding list should have exactly 6 items: `let`, <val_name>, `=`, `expression`, `in` and <expression>.\n");
                ASTNode item1 = vecGet(node.v.list.v, 1, ASTNode);
                if (item1.tag != ASTNODE_TAG_SYMBOL) {
                    print_error_in_text_at_char(input, item1.s_start,
                                                item1.s_end,
                                                "Let binding val_name should be a symbol.\n");
                }

                ASTNode item2 = vecGet(node.v.list.v, 2, ASTNode);
                if (!(item2.tag == ASTNODE_TAG_SYMBOL &&
                      strcmp(item2.v.symbol.data, "=") == 0)) {
                    print_error_in_text_at_char(input, item2.s_start,
                                                item2.s_end,
                                                "Let binding third item should be an `=`.\n");
                }

                ASTNode item3 = vecGet(node.v.list.v, 3, ASTNode);

                ASTNode item4 = vecGet(node.v.list.v, 4, ASTNode);
                if (!(item4.tag == ASTNODE_TAG_SYMBOL &&
                      strcmp(item4.v.symbol.data, "in") == 0)) {
                    print_error_in_text_at_char(input, item4.s_start,
                                                item4.s_end,
                                                "Let binding fifth item should be an `in`.\n");
                }

                ASTNode item5 = vecGet(node.v.list.v, 5, ASTNode);


                IRLambdaDef *ld = malloc(sizeof(IRLambdaDef));
                ld->arg_name = vecClone(item1.v.symbol);
                ld->body = compile_expr(input, item5);
                IRExpr lambda_expr = {.tag=EXPR_TAG_LAMBDA_DEF, .v.lambda_def=ld};

                IRFuncCall *fc = malloc(sizeof(IRFuncCall));
                fc->func_expr = lambda_expr;
                fc->arg = compile_expr(input, item3);
                IRExpr func_call_expr = (IRExpr) {.tag=EXPR_TAG_FUNC_CALL, .v.func_call=fc};

                return func_call_expr;
            }
        }

        // function call
        if (node.v.list.v.size < 2)
            print_error_in_text_at_char(input, node.s_start, node.s_end,
                                        "Function call list should have at least 2 items: <function_expression> and <argument_value>.\n");

        IRExpr result_expr;

        if (item0.tag == ASTNODE_TAG_SYMBOL) {
            // If a is a symbol, then
            // (a b)
            // Syntactic sugar for
            // ((getg a) b)

            result_expr = (IRExpr) {.tag=EXPR_TAG_GET_GLOBAL, .v.get_global=vecClone(
                item0.v.symbol)};
        } else {
            result_expr = compile_expr(input, item0);
        }

        for (size_t i = 1; i < node.v.list.v.size; ++i) {
            ASTNode i_item = vecGet(node.v.list.v, i, ASTNode);
            // call item0 with argument i_item
            IRFuncCall *fc = malloc(sizeof(IRFuncCall));
            fc->func_expr = result_expr;
            fc->arg = compile_expr(input, i_item);
            result_expr = (IRExpr) {.tag=EXPR_TAG_FUNC_CALL, .v.func_call=fc};
        }

        return result_expr;
    } else {
        panic("Compilation error!\n");
    }
}

IRProgram compile(const char *input, ASTNode node) {
    return (IRProgram) {.expr=compile_expr(input, node)};
}

IRExpr clone_ir_expr(IRExpr expr) {
    IRExpr result = {.tag=expr.tag};
    if (expr.tag == EXPR_TAG_VALUE) {
        result.v.value = clone_NValue(expr.v.value);
    } else if (expr.tag == EXPR_TAG_FUNC_CALL) {
        IRFuncCall *fc = malloc(sizeof(IRFuncCall));
        fc->func_expr = clone_ir_expr(expr.v.func_call->func_expr);
        fc->arg = clone_ir_expr(expr.v.func_call->arg);
        result.v.func_call = fc;
    } else if (expr.tag == EXPR_TAG_LAMBDA_DEF) {
        IRLambdaDef *ld = malloc(sizeof(IRLambdaDef));
        ld->arg_name = vecClone(expr.v.lambda_def->arg_name);
        ld->body = clone_ir_expr(expr.v.lambda_def->body);
        result.v.lambda_def = ld;
    } else if (expr.tag == EXPR_TAG_GET_GLOBAL) {
        result.v.get_global = vecClone(expr.v.get_global);
    } else {
        panic("Unknown expr tag.\n");
    }
    return result;
}

void free_ir_expr(IRExpr expr) {
    if (expr.tag == EXPR_TAG_VALUE) {
        free_NValue(expr.v.value);
    } else if (expr.tag == EXPR_TAG_FUNC_CALL) {
        free_ir_expr(expr.v.func_call->func_expr);
        free_ir_expr(expr.v.func_call->arg);
        free(expr.v.func_call);
    } else if (expr.tag == EXPR_TAG_LAMBDA_DEF) {
        vecDestroy(expr.v.lambda_def->arg_name);
        free_ir_expr(expr.v.lambda_def->body);
        free(expr.v.lambda_def);
    } else if (expr.tag == EXPR_TAG_GET_GLOBAL) {
        vecDestroy(expr.v.get_global);
    } else {
        panic("Unknown expression type.\n");
    }
}

void free_ir_program(IRProgram program) {
    free_ir_expr(program.expr);
}
