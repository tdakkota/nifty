#include "hashmap.h"

static u8 hash(const char *key) {
    u8 result = 0;
    for (size_t i = 0; key[i] != 0; ++i) {
        result ^= (u8) key[i];
    }
    return result;
}

Hashmap hashmapNew(void) {
    Hashmap hm = {};
    for (size_t i = 0; i < 256; i++) {
        hm.pairs[i] = vecNew();
    }
    return hm;
}

void hashmapAdd(Hashmap *self, const char *key, u64 v) {
    u8 key_hash = hash(key);
    // hm.pairs[key_hash]

    vecPush(self->pairs[key_hash], ((Pair) {.key=clone_str(key), .value=v}),
            Pair);
}

u64 hashmapGet(Hashmap *self, const char *key) {
    u8 key_hash = hash(key);
    // hm.pairs[key_hash]

    for (size_t i = 0; i < self->pairs[key_hash].size; i++) {
        Pair p = vecGet(self->pairs[key_hash], i, Pair);
        if (strcmp(key, p.key) == 0)
            return p.value;
    }

    panic("Key not found\n");
}

void hashmapDestroy(Hashmap *self) {
    for (size_t i = 0; i < 256; i++) {
        for (size_t j = 0; j < self->pairs[i].size; j++) {
            free(vecGet(self->pairs[i], j, Pair).key);
        }
        vecDestroy(self->pairs[i]);
    }
}

/*
static void test_hashmap(void) {
    Hashmap hs = hashmapNew();
    hashmapAdd(&hs, "kek", 100);
    hashmapGet(&hs, "kek");
    hashmapDestroy(&hs);
}
*/
