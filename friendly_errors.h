#include <stdlib.h>

void print_error_in_text_at_char(const char *string, size_t target_start,
                                 size_t target_end,
                                 const char *error);
